const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const { build } = require('../scripts/config')

console.log('using PRODUCTION webpack config')
module.exports = {
  target: 'node',
  mode: 'production',
  entry: build.entry,
  externals: build.externals(),
  output: {
    filename: '[name].js',
    chunkFilename: '[id].js',
    libraryTarget: 'commonjs2',
    path: build.output,
  },
  optimization: {
    moduleIds: 'hashed',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: build.babel,
            },
          },
        ],
      },
    ],
  },
  resolve: build.resolve,
  plugins: [
    new CopyWebpackPlugin(build.copy),
    new webpack.DefinePlugin(build.globals),
    new webpack.NormalModuleReplacementPlugin(/^es6-promisify$/, 'util'),
    new webpack.NormalModuleReplacementPlugin(/^any-promise$/, 'bluebird'),
    new webpack.NormalModuleReplacementPlugin(/^node-uuid$/, 'uuid'),
    new webpack.HashedModuleIdsPlugin(),
  ],
}
