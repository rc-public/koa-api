const path = require('path')
const RunNodeWebpackPlugin = require('run-node-webpack-plugin')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const { build } = require('../scripts/config')

console.log(`will run ${JSON.stringify(build.run())}`)
const scriptsToRun = build.run().map(
  script =>
    new RunNodeWebpackPlugin({
      scriptToRun: path.join(build.output, `${script}.js`),
    })
)

module.exports = {
  target: 'node',
  mode: 'development',
  devtool: 'inline-source-map',
  entry: build.entry,
  externals: build.externals(),
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    libraryTarget: 'commonjs2',
    path: build.output,
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude:/node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: build.babel,
            },
          },
          // { loader: 'eslint-loader' },
        ],
      },
    ],
  },
  resolve: build.resolve,
  plugins: [
    new CopyWebpackPlugin(build.copy),
    new webpack.DefinePlugin(build.globals),
    new webpack.NormalModuleReplacementPlugin(/^es6-promisify$/, 'util'),
    new webpack.NormalModuleReplacementPlugin(/^any-promise$/, 'bluebird'),
    new webpack.NormalModuleReplacementPlugin(/^node-uuid$/, 'uuid'),
    new webpack.NamedModulesPlugin(),
    ...scriptsToRun,
  ],
}
