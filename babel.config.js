const { camelCase } = require('lodash')

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
  ],
  plugins: [
    '@babel/transform-runtime',
    ['@babel/plugin-proposal-class-properties', { loose: false }],
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    '@babel/plugin-proposal-export-namespace-from',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-throw-expressions',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-syntax-import-meta',
  ],
  env: {
    production: {
      plugins: [
        'lodash',
        [
          'transform-imports',
          {
            async: {
              // eslint-disable-next-line no-template-curly-in-string
              transform: 'async/${member}',
              preventFullImport: true,
            },
            'date-fns': {
              transform: importName => `date-fns/${camelCase(importName)}`,
              preventFullImport: true,
            },
          },
        ],
      ],
    },
  },
  ignore: ['node_modules', 'dist'],
}
