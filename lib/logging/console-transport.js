import { transports, format } from 'winston'

const consoleTransport = options =>
  new transports.Console({
    colorize: true,
    format: format.combine(format.colorize(), format.simple()),
    json: true,
    prettyPrint: true,
    stringify: true,
    ...options,
  })

export default consoleTransport
