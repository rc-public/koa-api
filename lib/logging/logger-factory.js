// eslint-disable-next-line import/no-unresolved
import { performance } from 'perf_hooks'
import { curry, isFunction, reduce } from 'lodash'
import { format } from 'util'
import { createLogger as createWinston } from 'winston'
import humanizeDuration from 'humanize-duration'

import consoleTransport from './console-transport'

const defaults = {
  after: start => `after ${durationFrom(start)}`,
  error: start => `ERROR after ${durationFrom(start)}`,
  done: start => `DONE in ${durationFrom(start)}`,
  level: 'warn',
  levels: ['error', 'warn', 'info', 'verbose', 'debug', 'silly'],
  silent: false,
}

const isPromise = obj => isFunction(obj.then)

const durationFrom = (start, humanize = true) => {
  const duration = performance.now() - start
  if (humanize) {
    return humanizeDuration(duration, { round: true, units: ['ms'] })
  }
  return duration
}

const getPrefix = config => {
  if (config.prefix) {
    return config.prefix
  } else if (config.name) {
    return ({ namespace, label }) => `[${config.name}/${namespace} ${label}]`
  } else {
    return ({ namespace, label }) => `[${namespace} ${label}]`
  }
}

const loggerFactory = options => {
  const config = { ...defaults, ...options }
  const longestLevelLength = Math.max(...config.levels.map(l => l.length))

  const prefix = getPrefix(config)

  const winston = createWinston({
    level: config.level,
    transports: [consoleTransport({ silent: config.silent })],
  })

  const createLogger = curry((namespace, label) =>
    reduce(
      config.levels,
      (lLogger, level) => {
        lLogger[level] = (...args) =>
          winston[level](
            ' '.repeat(longestLevelLength - level.length) + prefix({ label, namespace }) + format('', ...args)
          )
        return lLogger
      },
      {}
    )
  )

  const promiseResult = (result, { start, logger, rethrow }) =>
    result
      .then(value => {
        logger.verbose(config.done(start), '(async)')
        return value
      })
      .catch(error => {
        logger.error(config.error(start), '(async)', error)
        if (rethrow) throw error
      })

  const createLogged = curry((namespace, label) => {
    const rethrow = true
    const logger = createLogger(namespace, `${label}()`)

    return wrapped => {
      return function() {
        const start = performance.now()
        logger.verbose('START')
        try {
          const result = Reflect.apply(wrapped, this, arguments)

          if (result && isPromise(result)) {
            return promiseResult(result, { start, logger, rethrow })
          }

          logger.verbose(config.done(start))
          return result
        } catch (error) {
          logger.error(config.error(start), error)
          if (rethrow) throw error
          return null
        }
      }
    }
  })

  const createBoth = (...args) => ({
    log: createLogger(...args),
    logged: createLogged(...args),
  })

  return {
    createLogged,
    createLogger,
    createBoth,
  }
}

export default loggerFactory
