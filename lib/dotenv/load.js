import { forEach, keys } from 'lodash'
import fs from 'fs'
import path from 'path'

import parse from './parse'

/*
 * Main entry point into dotenv. Allows configuration before loading .env
 * @param {Object} options - options for parsing .env file
 * @param {string} [options.path=.env] - path to .env file
 * @param {string} [options.encoding=utf8] - encoding of .env file
 * @returns {Object} parsed object or error
 */
const load = options => {
  options = {
    dotenvPath: path.resolve(process.cwd(), '.env'),
    encoding: 'utf8',
    overwrite: false,
    ...options,
  }

  try {
    // specifying an encoding returns a string instead of a buffer
    const parsed = parse(fs.readFileSync(options.dotenvPath, { encoding: options.encoding }))

    forEach(keys(parsed), key => {
      // eslint-disable-next-line  no-prototype-builtins
      if (!process.env.hasOwnProperty(key) || options.overwrite) {
        process.env[key] = parsed[key]
      }
    })

    return { parsed }
  } catch (error) {
    return { error }
  }
}

export default load
