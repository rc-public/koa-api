// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
module.exports = {
  apps: [
    {
      // exec_mode: 'cluster',
      // instances: 'max',
      exec_mode: 'fork',
      name: 'api',
      script: 'dist/api.js',
      watch: false,
    },
  ],
}
