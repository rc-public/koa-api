# QRCode Adapter KOA

## Scripts

You can execute the scripts with `yarn <script>` or with `npm run <script>`, but `yarn` is the recommended approach.

### Development

- `dev` Builds the application and run with development env, and watch for file changes
- `lint` Lints and formats the code. _Ensure the coded is formated and linted before creating merge requests_
- `serve` Executes de built version o `/dist` in `node`

### Production

- `start` Executes de built version o `/dist` in `node`
- `build` Builds the application with the production settings



## Caveats

### Running on Windows

On `router.js` there a call to `getRoutes`. It will use a glob pattern to get de sub folders and create the endpoints and events accordinly. This _do not work_ on windows.

- `getRoutes.js`: _`['home']`_
