const debug = require('debug')
const rimraf = require('rimraf')
const webpack = require('webpack')

const log = {
  error: debug('scripts:compile:error'),
  info: debug('scripts:compile'),
}

const config = require('./config')
// eslint-disable-next-line security/detect-non-literal-require
const webpackConfig = require(config.webpack.config)

log.info('START')

log.info('cleaning output folder')
rimraf.sync(config.build.output)

log.info('compiling....')
const compiler = webpack(webpackConfig)

compiler.run((err, stats) => {
  if (err) log.error(err)
  else log.info(stats.toString(config.webpack.stats))
  log.info('DONE')
})
