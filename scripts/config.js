const fs = require('fs')
const path = require('path')
const { filter, reduce, concat } = require('lodash')

const mainFolder = 'src'

const { join, resolve } = path
const res = (...p) => resolve(__dirname, '..', ...p)
const mainRes = (...p) => res(mainFolder, ...p)

const env = process.env.NODE_ENV
const modulesToRun = () => (process.env.RUN_MODULES || ['api']).split(',')

console.info(`NODE_ENV: ${env}`)
const envPath = env === 'production' ? 'prod' : 'dev'

const globals = {
  'process.env.NODE_ENV': JSON.stringify(env),
  __ENV__: JSON.stringify(env),
  __DEV__: env === 'development' ? 'true' : 'false',
}

const folders = {
  build: res('dist'),
  main: mainRes(),
  api: mainRes('api'),
  temp: res('node_modules/.cache/build'),
  webpack: res('webpack'),
}

const localFoldersRegex = /\.bin|\.cache|\.yarn-integrity|@lib/

const getModules = folder => {
  const modules = []
  const rootModules = fs.readdirSync(folder)

  rootModules.forEach(module => {
    if (module.startsWith('@')) {
      const subModulesFolder = path.join(folder, module)
      const subModules = fs.readdirSync(subModulesFolder)

      subModules.forEach(subModule => {
        modules.push(`${module}/${subModule}`)
      })
    } else {
      modules.push(module)
    }
  })

  return modules
}

const getExternals = () => {
  const packageModules = getModules(res('node_modules'))

  return reduce(
    filter(packageModules, mod => !localFoldersRegex.test(mod)),
    (modules, mod) => {
      modules[mod] = `commonjs ${mod}`
      return modules
    },
    {}
  )
}

exports.mainRes = mainRes

exports.res = res

exports.env = env

exports.build = {
  run: () => modulesToRun(),
  entry: {
    api: join(folders.api, 'index.js'),
  },
  externals: getExternals,
  globals: globals,
  output: folders.build,
  babel: res('babel.config.js'),
  copy: [{ from: mainRes('api/assets/favicon.ico'), to: 'assets/favicon.ico' }],
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [res('node_modules')],
  },
}

exports.webpack = {
  config: join(folders.webpack, `webpack.${envPath}.js`),
  stats: {
    assets: true,
    builtAt: true,
    children: false,
    chunks: false,
    colors: true,
    context: mainRes(),
    entrypoints: env === 'production',
    env: true,
    hash: env === 'production',
    modules: false,
    timings: true,
    version: false,
  },
}
