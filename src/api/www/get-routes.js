import glob from 'glob'
import { map } from 'lodash'

import logServices from '../services/log-services'

import { mainRes } from '../../../scripts/config'

const basePath = `${mainRes('api', 'www')}`

const pattern = `.${basePath}/**/endpoints.js`

const { log, logged } = logServices.createBoth('api/www', 'getRoutes')

const getRoutes = async () => {
  if(process.platform === 'win32') {
    return ['home']
  }
  log.verbose('searching pattern', pattern)
  const globPromise = new Promise((resolve, reject) => {
    glob(pattern, null, (err, matches) => {
      if (err) return reject(err)
      // eslint-disable-next-line security/detect-non-literal-regexp
      const regex = new RegExp(`(${basePath}/)(.*)(/endpoints.js)`)
      return resolve(map(matches, m => regex.exec(m)[2]))
    })
  })

  const matches = await globPromise

  log.info(`found ${JSON.stringify(matches)}`)
  return matches
}

export default logged(getRoutes)
