import { forEach, map, toLower } from 'lodash'
import Router from 'koa-router'
import logServices from '../services/log-services'

import getRoutes from './get-routes'
const { logged } = logServices.createBoth('api/www', 'router')

const createRouter = (prefix, endpoints) => {
  const router = new Router({ prefix })

  forEach(endpoints, endpoint => {
    const { method, route, handlers } = endpoint
    const verb = toLower(method)

    router[verb](route, ...handlers)
  })

  return router
}

const addRoute = async (route, mainRouter) => {
  // eslint-disable-next-line extra-rules/no-commented-out-code
  const { endpoints } = await import(/* webpackChunkName: "[request]" */ `./${route}/endpoints`)
  const prefix = route === 'home' ? '' : `/${route}`

  const router = createRouter(prefix, endpoints)
  mainRouter.use(router.routes(), router.allowedMethods())
}

const router = logged(async app => {
  const mainRouter = new Router()
  const routes = await getRoutes()

  const addingRoutesPromises = map(routes, route => addRoute(route, mainRouter))

  await Promise.all(addingRoutesPromises)

  app.use(mainRouter.routes()).use(mainRouter.allowedMethods())
})

export default router
