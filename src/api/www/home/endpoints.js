const endpoints = [
  {
    handlers: [
      ctx => {
        ctx.body = 'QRCode KOA'
      },
    ],
    method: 'GET',
    route: '/',
  },
]

export { endpoints }
