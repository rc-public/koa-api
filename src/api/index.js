import { server as serverConfig } from './config'
import logServices from './services/log-services'

import { create as createHandler } from './handler'
import { create as createServer } from './server'

const { log, logged } = logServices.createBoth('api')


const api = async () => {
  const { schema, domain, port, protocol } = serverConfig

  const handler = await createHandler()
  const server = createServer(handler.callback(), protocol)

  server.listen(port, async error => {
    const serverLog = log('server')
    if (error) {
      serverLog.error(error)
      // eslint-disable-next-line unicorn/no-process-exit
      process.exit(1)
    } else {
      serverLog.info(`serving at ${schema}://${domain}:${port}`)
    }
  })
}

logged('main')(api)()
