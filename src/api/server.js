import logServices from './services/log-services'
import http from 'http'

const { log, logged } = logServices.createBoth('api', 'server')


const create = logged(app => {
  const server = http.createServer(app)

  server.on('error', err => log.error(err))
  server.on('socketError', err => log.error(err))

  return server
})

export { create }
