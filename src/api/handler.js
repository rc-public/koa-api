import bodyParser from 'koa-bodyparser'
import compress from 'koa-compress'
import helmet from 'koa-helmet'
import Koa from 'koa'
import logger from 'koa-logger'
import favicon from 'koa-favicon'
import path from 'path'
import logServices from './services/log-services'
import cors from '@koa/cors'

import router from './www/router'

const { log } = logServices.createBoth('api', 'handler')

const create = async () => {
  const app = new Koa()

  app.use(
    bodyParser({
      jsonLimit: '50mb',
    })
  )

  app.use(
    logger({
      transporter: (str, args) => {
        log.info(str)
      },
    })
  )


  await router(app)

  app.use(helmet())
  app.use(
    cors({
      allowMethods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
      origin: '*',
    })
  )

  app.use(compress())
  app.use(favicon(path.join('dist', 'assets', 'favicon.ico')))

  return app
}

export { create }
