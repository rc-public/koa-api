import dotenv from '@lib/dotenv'
import { includes } from 'lodash'

dotenv.load({ overwrite: true })

export const domain = process.env.DOMAIN
export const env = process.env.NODE_ENV

const protocol = process.env.PROTOCOL || 'http'
export const server = {
  port: process.env.PORT || 3000,
  schema: protocol === 'http' ? 'http' : 'https',
  domain: process.env.DOMAIN || 'localhost',
  protocol,
}

export const log = {
  level: process.env.LOGGER_LEVEL || 'debug',
  silent: includes(process.argv, '--silent'),
}

