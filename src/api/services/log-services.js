import { loggerFactory } from '@lib/logging'
import { log as logConfig } from '../config'

const logServices = loggerFactory({
  name: 'smart-fridge-api',
  ...logConfig,
})

export default logServices
